/*
 * Free Scale Model Development
 * This is part of the FSMD Project
 *
 * Copyright (c) 2020 Andreas Heckt
 *
 * This program is free software: you can redistribute it and/or modify  
 * it under the terms of the GNU General Public License as published by  
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License 
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Signal fundament small version according to BZA drawing S 8240.11.7
 * Signalfundament kleine Bauform nach BZA-Zeichnung S 8240.11.7
 *
 * Ressources/Quellen
 * [1]: https://www.beweka.de/fileadmin/pdf/Bauteile_fuer_Signalanlagen.pdf
 * [2]: Measures taken from originl part
 */

faktor = 1 / 87; // Maßstab
anzahl = 0;	// Anzahl Aufstockelemente (Zwischenstücke)


// Maße in mm

// allgemeine Maße
grundkoerper_l = 400; // Länge
grundkoerper_h = 450; // Höhe

// Montage
$fn = $preview == true ? 64 : 128;

translate([0, 0, 1]) scale(faktor) {
	translate([0, 0, grundkoerper_h * max(anzahl, 0)]) fundament();
	if (anzahl > 0) for ( i = [0 : 1 : anzahl - 1] ) {
		translate([0, 0, grundkoerper_h * i]) aufstocker();
	}
	base();
}

// Oberteil
module fundament() {
	// Betongrundkörper
	oberteil_l = grundkoerper_l; // Länge
	oberteil_b = oberteil_l; // Breite
	oberteil_h = grundkoerper_h - 80; // Höhe
	oberteil_z = oberteil_h / 2; // z-Position ggü. Nullpunkt

	// Eckausschnitt
	ecke_l = 105; // Länge
	ecke_b = ecke_l; // Breite
	ecke_h1 = 80; // Höhe gesamt
	ecke_h2 = 50; // Höhe schräger Teil
	ecke_x = (oberteil_l - ecke_l) / 2; // x-Position ggü. Nullpunkt
	ecke_y = ecke_x; // y-Position ggü. Nullpunkt
	ecke_z = oberteil_h - (ecke_h1 - ecke_h2) / 2; // z-Position ggü. Nullpunkt

	// Löcher für Gewindestangen
	loch_d = 0.6 / faktor; // Durchmesser
	loch_h = oberteil_h; // Höhe
	loch_x = (oberteil_l - ecke_l) / 2; // x-Position ggü. Nullpunkt
	loch_y = loch_x; // y-Position ggü. Nullpunkt
	loch_z = loch_h / 2; // x-Position ggü. Nullpunkt

	// Unterlegplatten für Gewindestangen
	platte_d = 0.3 / faktor; // Durchmesser; angepasst an Fertigungsmöglichkeiten von ELEGOO Mars
	platte_l = 70; // Länge
	platte_b = platte_l; // Breite
	platte_h = 0.5 / faktor; // Höhe
	platte_x = (oberteil_l - ecke_l) / 2; // x-Position ggü. Nullpunkt
	platte_y = platte_x; // y-Position ggü. Nullpunkt
	platte_z = oberteil_h - platte_h / 2 + 8; // z-Position ggü. Nullpunkt

	difference() {
		// Betongrundkörper
		union() {
			translate([0, 0, oberteil_z]) cube([oberteil_l, oberteil_b, oberteil_h], center = true);

			for ( alpha = [0 : 90 : 90] ) {
				rotate([0, 0, alpha]) hull() {
					translate([0, 0, oberteil_h + (ecke_h1 - ecke_h2) / 2]) cube([oberteil_l, oberteil_b - 2 * ecke_b, ecke_h1 - ecke_h2], true);
					translate([0, 0, oberteil_h + ecke_h1 - ecke_h2 / 2]) cube([oberteil_l - 30, oberteil_b - 2 * ecke_b - 30, ecke_h2], true);
				}
			}

			// Unterlegplatten für Gewindestangen
			for ( x = [-1 : 2 : 1] ) {
				for ( y = [-1 : 2 : 1] ) {
					translate([platte_x * x, platte_y * y, platte_z]) difference() {
						cube([platte_l, platte_b, platte_h], center = true);
						cylinder(r = platte_d / 2, h = platte_h, center = true);
					}

				}
			}
		}

		// Löcher für Gewindestangen
		for ( x = [-1 : 2 : 1] ) {
			for ( y = [-1 : 2 : 1] ) {
				translate([loch_x * x, loch_y * y, loch_z]) cylinder(r = loch_d / 2, h = loch_h, center = true);
			}
		}

		// Querkanal
		querkanal(oberteil_l);

		// Schacht senkrecht
		schacht(oberteil_h + ecke_h1);
	}	
}

// Aufstockelement
module aufstocker() {
	// Betongrundkörper
	oberteil_l = grundkoerper_l; // Länge
	oberteil_b = oberteil_l; // Breite
	oberteil_h = grundkoerper_h - 5; // Höhe
	oberteil_z = oberteil_h / 2; // z-Position ggü. Nullpunkt

	// Löcher für Gewindestangen
	loch_d = 0.6 / faktor; // Durchmesser
	loch_h = oberteil_h + 5; // Höhe
	loch_x = (oberteil_l - 105) / 2; // x-Position ggü. Nullpunkt
	loch_y = loch_x; // y-Position ggü. Nullpunkt
	loch_z = loch_h / 2; // x-Position ggü. Nullpunkt

	difference() {
		// Betongrundkörper
		hull() {
			translate([0, 0, oberteil_z]) cube([oberteil_l, oberteil_b, oberteil_h], center = true);
			translate([0, 0, oberteil_h + 5 / 2]) cube([oberteil_l - 5 * 2, oberteil_b - 5 * 2, 5], center = true);
		}

		// Löcher für Gewindestangen
		for ( x = [-1 : 2 : 1] ) {
			for ( y = [-1 : 2 : 1] ) {
				translate([loch_x * x, loch_y * y, loch_z]) cylinder(r = loch_d / 2, h = loch_h, center = true);
			}
		}

		// Querkanal
		querkanal(oberteil_l);

		// Schacht senkrecht
		schacht(oberteil_h + 5);
	}
}

// Montagesockel
module base() {
	// Platte
	platte_l = 8 / faktor; // Länge
	platte_b = platte_l; // Breite
	platte_h = 1 / faktor; // Höhe
	platte_z = -platte_h / 2; // z-Position ggü. Nullpunkt

	// Zylinder außen
	zylinder_a_d = 4 / faktor; // Durchmesser
	zylinder_a_h = 5 / faktor; // Höhe
	zylinder_a_z = -zylinder_a_h / 2 - platte_h; // z-Position ggü. Nullpunkt

	// Zylinder innen
	zylinder_i_d = zylinder_a_d - 2 / faktor; // Durchmesser
	zylinder_i_h = zylinder_a_h + platte_h; // Höhe
	zylinder_i_z = -zylinder_i_h / 2; // z-Position ggü. Nullpunkt

	// Klebeöffnung für 'Gewindestäbe'
	loch_d = 0.6 / faktor; // Durchmesser
	loch_h = platte_h * 2; // Höhe
	loch_x = (grundkoerper_l - 105) / 2; // x-Position ggü. Nullpunkt
	loch_y = loch_x;  // x-Position ggü. Nullpunkt
	loch_z = -loch_h / 2;  // z-Position ggü. Nullpunkt

	difference() {
		union() {
			// Platte
			translate([0, 0, platte_z]) cube([platte_l, platte_b, platte_h], true);

			// Zylinder außen
			translate([0, 0, zylinder_a_z]) cylinder(r = zylinder_a_d / 2, h = zylinder_a_h, center = true);
		}
		
		// Zylinder innen
		translate([0, 0, zylinder_i_z]) cylinder(r = zylinder_i_d / 2, h = zylinder_i_h + 1, center = true);

		// Klebeöffnung für 'Gewindestäbe'
		for ( x = [-1 : 2 : 1] ) {
			for ( y = [-1 : 2 : 1] ) {
				translate([loch_x * x, loch_y * y, loch_z]) cylinder(r = loch_d / 2, h = loch_h + 1, center = true);
			}
		}
	}
}

// Querkanal
module querkanal(length) {
	b = 100; // Breite
	h = 170; // Höhe
	l = length; // Länge

	translate([0, -l / 2, 0]) rotate([0, 90, 90]) linear_extrude(l) translate([-(h - b / 2), 0, 0]) union() {
		circle(r = b / 2);
		translate([(h - b / 2) / 2, 0, 0]) square(size = [h - b / 2, b], center = true);
	}
}

// Schacht senkrecht
module schacht(height) {
	l = grundkoerper_l - 2 * 105 - 30; // Länge
	b = l - 50; // Breite
	h = height; // Höhe

	linear_extrude(h) hull() {
		square(size = [l, b], center = true);
		square(size = [b, l], center = true);
	}
}